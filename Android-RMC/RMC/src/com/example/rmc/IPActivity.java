/*Application Name: RMC
Version: 0.0.1(Beta version)
Developer&Maintainer: Swapnil J. Udapure
Repository: https://github.com/swapgit/swap-repo/RMC
E-mail: swapnil.udapure5@gmail.com*/

package com.example.rmc;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
 
@SuppressLint("NewApi")
public class IPActivity extends Activity{
 
 Button button;//,button10;
 EditText edtxt;
 static AlertDialog alertDialog;
 
 @SuppressLint("NewApi")
@Override
 public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.ip_layout);

  button = (Button)findViewById(R.id.button);   //reference to the send button
 
  ActionBar bar = getActionBar();
  	bar.setBackgroundDrawable(new ColorDrawable(Color.DKGRAY));
  edtxt = (EditText) findViewById(R.id.ip);
  alertDialog = new AlertDialog.Builder(this).create();
	 alertDialog.setTitle("RMC...");
	 
	 button.setOnClickListener(new View.OnClickListener() {
 
   public void onClick(View v) {
	   		if(!edtxt.getText().toString().equals(""))
	   		{
	   			Intent i = new Intent(IPActivity.this, MainActivity.class);
        		i.putExtra("ip", edtxt.getText().toString());
    			startActivity(i);
	   		}
	   		else
	   		{
	   			alertDialog.setMessage("Please Enter IP First");
	   		    alertDialog.show();
	   		}
   }
  }); 
  
 }
 
 protected void onResume() {
     super.onResume();
 }

 protected void onPause() {
     super.onPause();
 }
 
}

